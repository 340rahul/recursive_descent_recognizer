/**
 * CSE 305 Spring 2015
 * Assignment #2: Object-Oriented Top-down Parsing - Part I
 * 
 * @author Amal Khandelwal
 * @author Rahul Tripathi
 *
 */

@SuppressWarnings("serial")
class Redeclaration extends Exception {
	public String message;

	public Redeclaration(String s) {
		super(s);
		message = s;
	}
}

@SuppressWarnings("serial")
class TypeUnknown extends Exception {
	public String message;

	public TypeUnknown(String s) {
		super(s);
		message = s;
	}
}

@SuppressWarnings("serial")
class TypeMismatch extends Exception {
	public String message;

	public TypeMismatch(String s) {
		super(s);
		message = s;
	}
}

public class TinyPL {
	// symbol-table for 26 possible single letter identifiers
	public static java.util.HashMap<Character, String> ST =
			new java.util.HashMap<Character, String>(26);

	public static void main(String args[]) {
		Lexer.lex();
		try {
			new Program();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

/**
 * program -> 'begin' decls stmts 'end'
 */
class Program {
	Decls d;
	Stmts s;

	public Program() throws Redeclaration {
		if (Lexer.nextToken == Token.KEY_BEGIN) {
			Lexer.lex(); // 'begin'
			d = new Decls();
			s = new Stmts();
			// not consuming 'end' as no more lexical tokens exist after it
		}
	}
}

/**
 * decls -> ['int' idlist ';'] ['real' idlist ';'] ['bool' idlist ';']
 */
class Decls {
	Idlist i, r, b;

	public Decls() throws Redeclaration {
		if (Lexer.nextToken == Token.KEY_INT) {
			Lexer.lex(); // 'int'
			i = new Idlist("int");
			Lexer.lex(); // ';'
		}
		if (Lexer.nextToken == Token.KEY_REAL) {
			Lexer.lex(); // 'real'
			r = new Idlist("real");
			Lexer.lex(); // ';'
		}
		if (Lexer.nextToken == Token.KEY_BOOL) {
			Lexer.lex(); // 'bool'
			b = new Idlist("bool");
			Lexer.lex(); // ';'
		}
	}
}

/**
 * idlist -> id {',' id}
 */
class Idlist {
	Id_Lit i;
	Idlist il;
	String type;

	public Idlist(String t) throws Redeclaration {
		type = t;

		if (Lexer.nextToken == Token.ID) {
			Lexer.lex(); // id

			// check if the variable has already been declared
			if (TinyPL.ST.containsKey(Lexer.ident)) {
				throw new Redeclaration(Character.toString(Lexer.ident));
			} else {
				i = new Id_Lit();
				i.type = type;
				TinyPL.ST.put(i.id, type);
			}

			if (Lexer.nextToken == Token.COMMA) {
				Lexer.lex(); // ','
				il = new Idlist(type);
			}
		}
	}
}

/**
 * stmts -> [stmt [stmts]]
 */
class Stmts {
	Stmt s;
	Stmts ss;

	public Stmts() {
		if (isStmt(Lexer.nextToken)) {
			s = new Stmt();

			if (isStmt(Lexer.nextToken))
				ss = new Stmts();
		}
	}

	private boolean isStmt(final int token) {
		switch (token) {
		case Token.ID:
		case Token.KEY_IF:
		case Token.KEY_WHILE:
		case Token.LEFT_BRACE:
			return true;
		}
		return false;
	}
}

/**
 * stmt -> assign | cond | loop | cmpd
 */
class Stmt {
	Assign a;
	Cond c;
	Loop l;
	Cmpd cm;

	public Stmt() {
		// token consumed in the respective constructors
		switch (Lexer.nextToken) {
		case Token.ID:
			a = new Assign();
			break;
		case Token.KEY_IF:
			c = new Cond();
			break;
		case Token.KEY_WHILE:
			l = new Loop();
			break;
		case Token.LEFT_BRACE:
			cm = new Cmpd();
			break;
		}
	}
}

/**
 * assign -> var '=' expr ';'
 */
class Assign {
	Id_Lit i;
	Expr e;

	public Assign() {
		Lexer.lex(); // var (id)

		i = new Id_Lit();

		if (Lexer.nextToken == Token.ASSIGN_OP) {
			Lexer.lex(); // '='
			e = new Expr();
			Lexer.lex(); // ';'
		}
	}
}

/**
 * cond -> 'if' '(' expr ')' stmt ['else' stmt]
 */
class Cond {
	Expr e;
	Stmt s_if;
	Stmt s_el;

	public Cond() {
		Lexer.lex(); // 'if'

		if (Lexer.nextToken == Token.LEFT_PAREN) {

			/*
			 * parenthesis surrounding expr are consumed in Factor to avoid:
			 * if ((x == y))
			 */

			e = new Expr();
			s_if = new Stmt();

			if (Lexer.nextToken == Token.KEY_ELSE) {
				Lexer.lex(); // 'else'
				s_el = new Stmt();
			}
		}
	}
}

/**
 * loop -> 'while' '(' expr ')' stmt
 */
class Loop {
	Expr e;
	Stmt s;

	public Loop() {
		Lexer.lex(); // 'while'

		if (Lexer.nextToken == Token.LEFT_PAREN) {

			/*
			 * parenthesis surrounding expr are consumed in Factor to avoid:
			 * while ((x == y))
			 */

			e = new Expr();
			s = new Stmt();
		}
	}
}

/**
 * cmpd -> '{' stmts '}'
 */
class Cmpd {
	Stmts s;

	public Cmpd() {
		Lexer.lex(); // '{'
		s = new Stmts();
		Lexer.lex(); // '}'
	}
}

/**
 * expr -> term [('+' | '-' | '||') expr]
 */
class Expr {
	Term t;
	Expr e;
	String type;

	public Expr() {
		t = new Term();
		type = t.type;

		switch (Lexer.nextToken) {
		case Token.ADD_OP:
		case Token.SUB_OP:
		case Token.OR_OP:
			Lexer.lex(); // ('+' | '-' | '||')
			e = new Expr();
		}
	}
}

/**
 * term -> factor [('*' | '/' | '&&') term]
 */
class Term {
	Factor f;
	Term t;
	String type;

	public Term() {
		f = new Factor();
		type = f.type;

		switch (Lexer.nextToken) {
		case Token.MULT_OP:
		case Token.DIV_OP:
		case Token.AND_OP:
			Lexer.lex(); // ('*' | '/' | '&&')
			t = new Term();
		}
	}
}

/**
 * factor -> literal
 *           | '!' factor
 *           | '(' expr ')'
 *           | '(' expr ('<=' | '>=' | '==' | '!=' | '<' | '>') expr ')'
 */
class Factor {
	Int_Lit i;
	Real_Lit r;
	Bool_Lit b;
	Id_Lit id;
	Expr e1, e2;
	Factor f;
	String type;

	public Factor() {
		/*
		 * calling Lexer.lex after instantiation of literals since
		 * the value of nextToken is needed to determine the type
		 */
		switch (Lexer.nextToken) {
		case Token.INT_LIT:
			Lexer.lex();
			type = "int";
			i = new Int_Lit();
			break;
		case Token.REAL_LIT:
			Lexer.lex();
			type = "real";
			r = new Real_Lit();
			break;
		case Token.TRUE_LIT:
		case Token.FALSE_LIT:
			type = "bool";
			b = new Bool_Lit();
			Lexer.lex();
			break;
		case Token.ID:
			
			id = new Id_Lit();
			Lexer.lex();
			break;
		case Token.NEG_OP:
			Lexer.lex(); // '!'
			f = new Factor();
			break;
		case Token.LEFT_PAREN:
			Lexer.lex(); // '('
			e1 = new Expr();
			type = e1.type;

			// check if the Expr above is followed by a relop
			switch (Lexer.nextToken) {
			case Token.LE_OP:
			case Token.GE_OP:
			case Token.EQ_OP:
			case Token.NE_OP:
			case Token.LT_OP:
			case Token.GT_OP:
				Lexer.lex(); // ('<=' | '>=' | '==' | '!=' | '<' | '>')
				e2 = new Expr();
			}

			Lexer.lex(); // ')'
			break;
		}
	}
}

/**
 * literal -> int_lit | real_lit | 'true' | 'false' | id
 */
class Literal {
	String type;

	public Literal() {
	}
}

class Int_Lit extends Literal {
	int i;

	public Int_Lit() {
		i = Lexer.intValue;
	}
}

class Real_Lit extends Literal {
	double r;

	public Real_Lit() {
		r = Lexer.realValue;
	}
}

class Bool_Lit extends Literal {
	boolean b;

	public Bool_Lit() {
		b = Lexer.nextToken == Token.TRUE_LIT;
	}
}

class Id_Lit extends Literal {
	final char id;

	public Id_Lit() {
		id = Lexer.ident;
	}
}
